<?php

require_once('_config.php');
//Verification des droits d'administrateur
if (!isset($_SESSION['userId'])) {
  add_flash('warning', 'Veuillez vous connecter ou créer un compte');
  header('Location: ' . $root_url . 'login.php');
  die;
}else{
  $query='SELECT * FROM user WHERE id='.$_SESSION['userId'];
  $resadmin = $pdo->query($query, PDO::FETCH_CLASS, "User")->fetch();
  $resadmin->getId();
  if(!$resadmin->getAdmin()){
    add_flash('warning', 'Vous devez avoir un compte administrateur pour avoir accès à cette page');
    header('Location: ' . $root_url . 'index.php');
    die;
  }
}
//Supression de l'article
if (isset($_GET['report'])) {
  $idart=$pdo->quote($_GET['report']);
  $pdo->exec('DELETE FROM annonces WHERE id='.$idart);
  $pdo->exec('DELETE FROM report WHERE annonce_id='.$idart);
  $pdo->exec('DELETE FROM favorit WHERE annonce_id='.$idart);
  add_flash('warning', 'Article supprimé');
  header('Location: ' . $root_url . 'index.php');
  die;

}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>L'AngleAgréable</title>
  <?php include('_head.php') ?>
</head>

<body>
  <?php include('_header.php') ?>
  <!-- Affichage des articles ayant été signaler -->
  <?php $reports=($pdo->query('SELECT annonces.*,count(*) as nbreport FROM annonces,report WHERE annonces.id=report.annonce_id GROUP BY report.annonce_id ORDER BY nbreport DESC',PDO::FETCH_CLASS, "articlereport" ));?>
  <div class="container">
    <article>
      <div class="card mb-3 border-info">
        <div class="card-header text-white bg-info">Signalement d'annonces</div>
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nom</th>
              <th scope="col">NbReport</th>
              <th scope="col">Supprimer</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($reports as $report):?>
            <tr>
              <th scope="row"><?php echo $report->getId() ?>
              </th>
              <td>
                <a href="post.php?id=<?php echo $report->getSlug() ?>">
                  <?php echo $report->getTitle() ?>
                </a>  
              </td>
              <td>
              <p>
                  <?php echo $report->getNbreport() ?>
                </p>
              </td>
              <td>
                <a href="admin.php?id=<?php echo $report->getId()?>"
                class="btn btn-danger"
                title="<?php echo $report->getTitle();  ?>">
                <?php echo "Supprimer l'article";  ?></a>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </article>
  </div>

  <?php include('_footer.php') ?>
</body>

</html>