<?php require_once('_config.php') ;
$user = $pdo->query('SELECT * FROM user WHERE user.id ='.$pdo->quote($_SESSION['userId']), PDO::FETCH_CLASS, "user")->fetch();?>

<?php if (!isset($_SESSION['userId'])) {
    add_flash('warning', 'Veuillez vous connecter ou créer un compte');
    header('Location: '.$root_url.'login.php');
    die;
}
?>

<?php if (isset($_POST["sendpass"])) {
    remove_flash();
    
    if (password_verify($_POST["pass_old"].$user->getPseudo(), $user->getPass())) {
        $options = [ //Sur les versions de php >= 5,7 le sel se genere automatiquement
            'cost' => 11
        ];
        
        $hash = password_hash($_POST["pass1"].$user->getPseudo(), PASSWORD_BCRYPT, $options);
        $rs = $pdo->query('UPDATE user SET user.pass = "' .$hash. '" WHERE user.id='.$user->getId());
        if ($rs) {
            add_flash('success', 'Password updated');
        } else {
            add_flash('warning', 'Error');
        }
    } else {
        add_flash('warning', 'Invalid password');
    }
    header('Location: '.$root_url."account.php"); //On redirige sur la meme page pour eviter une submisson du post sur le refresh (Post/Redirect/Get)
    die;
} elseif (isset($_POST["sendmail"])) {
    $rs = $pdo->query('UPDATE user SET user.email = ' .$pdo->quote($_POST["mail"]). ' WHERE user.id='.$user->getId());
    if ($rs) {
        add_flash('success', 'Email updated');
    } else {
        add_flash('warning', 'Error');
    }
    header('Location: '.$root_url."account.php"); //On redirige sur la meme page pour eviter une submisson du post sur le refresh (Post/Redirect/Get)
    die;
}
?>
<?php if (isset($_POST['perso'])) {
    if ($user->getPersonnalinf()) {
        $pdo->exec('UPDATE user SET personnal_inf=0 WHERE id='.$pdo->quote($_SESSION['userId']));
    }else{ 
        $pdo->exec('UPDATE user SET personnal_inf=1 WHERE id='.$pdo->quote($_SESSION['userId']));
    } 

}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>L'AngleAgréable</title>
    <?php include('_head.php') ?>
</head>

<body>

    <?php include('_header.php'); ?>
    <?php show_flash(); ?>
    <div class="container">

        <h2>
            <?php echo $user->getPseudo() ?>
        </h2>

        <article>
            <form action="" method="POST">
                <div class="card mt-3 mb-3">
                    <div class="card-body">
                        <p class="card-text"><?php echo $user->getEmail() ?>
                        </p>
                        <p> <input type="submit" name='editmail' id="mail" class="btn btn-primary"
                                value="Change mail adress" formnovalidate> </p>

                         <!-- Ajout des entrées pour changer de mail -->
                        <?php if (isset($_POST["editmail"]) ) : ?>

                            <div class="form-group">
                                <label for="mail">Email</label>
                                <input type="email" class="form-control" id="mail" name="mail" required />
                            </div>
                            <p> <input type="submit" name='sendmail' class="btn btn-primary" value="Send new email"> </p>

                        <?php endif ?>

                        <p> <input type="submit" name='editpassword' id="password" class="btn btn-primary"
                                value="Change password" formnovalidate> </p>

                        <!-- Ajout des entrées pour changer de mot de passe -->
                        <?php if ( isset($_POST["editpassword"])) : ?>

                            <div class="form-group">
                                <label for="commentaire">Old Password</label>
                                <input type="password" class="form-control" id="password0" name="pass_old" minlength="8"
                                    required />
                            </div>

                            <div class="form-group">
                                <label for="commentaire">New Password</label>
                                <input type="password" class="form-control" id="password1" name="pass1" minlength="8"
                                    required />
                            </div>

                            <div class="form-group">
                                <label for="commentaire"> Confirm new password</label>
                                <input type="password" class="form-control" id="password2" name="pass2" minlength="8"
                                    required />
                            </div>

                            <p> <input type="submit" name='sendpass' class="btn btn-primary" value="Send new password"> </p>

                        <?php endif ?>
                <!-- Bouton pour changer l'affichage des données personelles -->
                        <div>
                            <input type="submit" name='perso' id="perso" class="btn btn-primary"
                            <?php if ($user->getPersonnalinf()) {?>
                                    value="Ne plus afficher mon nom,prénom,téléphone"  
                            <?php }else{ ?>
                                    value="Afficher mon nom,prénom,téléphone" 
                            <?php } ?>
                                    />
                        </div>

                    </div>
                </div>
            </form>
        </article>
    </div>

    <?php include('_footer.php') ?>
</body>

</html>

<script type="text/javascript">
    var password = document.getElementById("password1"),
        confirm_password = document.getElementById("password2");

    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onchange = validatePassword;
</script>